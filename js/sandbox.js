Kernel.sandbox.define('main', {
	/*******  Module Properties  *******/
	mRunningList: [],
	mRunningTitle: '',
	mRunningFSA: '',
		
				
	refreshStatusFeed: function() {
		var self = this;
		
		Kernel.doREST({
			type: 'GET',
			url: 'feed',
			success: function(data) {
				self.broadcast('status-feed-update', data);
			},
			failure: function(er) {
				
				// NOTE: No module in this example listens for error messages to errors will go unnoticed
				self.broadcast('error', "Couldn't fetch the feed: "+er);
			}
		});
	},
	
	updateStatus: function(status) {
		var self = this;
		
		Kernel.doREST({
			type: 'POST',
			url: 'status',
			params: { newstatus: status },
			success: function(data) {
				
				// Let everyone know that you updated your status
				self.broadcast('status-update', status);
				
				// Also update the feed
				self.refreshStatusFeed();
			},
			failure: function(er) {
				self.broadcast('error', "Couldn't update the status: "+er);
			}
		});
	}
});
    

