<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

		<!-----------------------------------------------------------
		  Include Dashboard Framework & Libraries
		  
		  For all <script> tags:
		  Use 'defer' attribute for script with DOM manipulation.
		  Use 'async' attribute for scripts without DOM manipulation.
		  All base libraries should not use either of these two attribs.
		------------------------------------------------------------>

		<link rel="stylesheet" href="css/jquery-ui.min.css">
		<link rel="stylesheet" href="css/jquery-ui.theme.min.css">
		<link rel="stylesheet" href="css/alertify.core.css">
		<link rel="stylesheet" href="css/alertify.default.css">
		<link rel="stylesheet" href="css/font-awesome.css">

		<link rel="stylesheet" type="text/css" href="css/style.css" />

		<script src="js/jquery.js"> </script>
		<script src="js/jquery-ui.min.js"></script>

		<script src="js/kernel.js"></script>
		<script src="js/kernel-extension.js"></script>
		<script src="js/sandbox.js"></script>
		<script src="js/notify.js" defer></script>
		
		<!-----------------------------------------------------------
		  Include Dashboard Widget Scripts/Styles
		  Scripts should use 'defer' attribute to increase perf.
		------------------------------------------------------------>

		<script src="widgets/statusbar/js/statusbar.js" defer></script>
		<link rel="stylesheet" type="text/css" href="widgets/statusbar/css/statusbar.css" />

		<!-----------------------------------------------------------
		  Launch Dashboard Widgets
		  (make sure all modules are "included" before you "launch" them)
		------------------------------------------------------------>
		<script>		  					
			$('document').ready(function() {			
				var x_width = window.innerWidth;
				var y_height = window.innerHeight;
				
				Kernel.register('widget-statusBar', 'statusbar');

				Kernel.start('widget-statusBar', {renderTo: 'statusBarContent'});			
		
			});
		</script>
	</head>
	<body>

  	<!--------------------------------------------------------------
	BODY BEGIN 
	Setup layers for the dashboard widgets
	--------------------------------------------------------------->
	<div id='statusBarLyr' class='statusBar'></div>

  </body>
</html>    

